# Installation
pip install -r requirements.txt

# Run server
foreman start
python manage.py runserver

# Deployment
git push heroku master