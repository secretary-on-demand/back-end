from django.forms import widgets
from rest_framework import serializers
from backend.models import Document, LANGUAGE_CHOICES, STYLE_CHOICES


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ('id', 'text')