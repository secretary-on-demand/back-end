from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from backend import views

router = routers.DefaultRouter()
router.register(r'document', views.DocumentViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'API.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	url(r'^', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
)
